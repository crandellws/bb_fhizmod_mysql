
	// =======================================================================================
	// CLASS CONSTANTS
	//
	// This is just to keep all constants in one place
	// =======================================================================================

	public static class Constants {
		
		public const string MYSQL_SERVER 	= "localhost";
		public const string MYSQL_USER 		= "yourMySqlUserName";
		public const string MYSQL_CHARSET 	= "utf8";
		public const string MYSQL_DATABASE 	= "yourMySqlDatabaseName";
		public const string MYSQL_PASSWORD 	= "yourMySqlDatabasePassword";
		public const int MYSQL_PORT 		= 3306; 

	}
