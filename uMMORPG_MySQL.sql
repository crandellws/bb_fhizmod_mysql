-- For use in a tool like MySQL Workbench to create the database and tables

-- The UTF-8 character set is recommended (Latin-1 is not compatible) 
CREATE DATABASE IF NOT EXISTS ummorpg
	DEFAULT CHARACTER SET utf8
	DEFAULT COLLATE utf8_general_ci;

-- Uncomment DROP TABLE to delete the table if it exists and needs to be replaced

-- DROP TABLE `characters`;    

CREATE TABLE IF NOT EXISTS `characters` (
	`name` VARCHAR(16) NOT NULL,
	`account` VARCHAR(16) NOT NULL,
	`class` VARCHAR(16) NOT NULL,
	`x` FLOAT NOT NULL,
	`y` FLOAT NOT NULL,
	`z` FLOAT NOT NULL,
	`level` INT NOT NULL,
	`hp` INT NOT NULL,
	`mp` INT NOT NULL,
	`strength` INT NOT NULL,
	`intelligence` INT NOT NULL,
	`exp` BIGINT NOT NULL,
	`skillExp` BIGINT NOT NULL,
	`gold` BIGINT NOT NULL,
	`coins` BIGINT NOT NULL,
	`deleted` TINYINT UNSIGNED NOT NULL,
    PRIMARY KEY(`name`),
    INDEX(`account`(16))
) ENGINE = InnoDB;
    
-- DROP TABLE `character_inventory`;

CREATE TABLE IF NOT EXISTS `character_inventory` (
	`character` VARCHAR(16) NOT NULL,
	`slot` INT NOT NULL,
	`name` VARCHAR(50) NOT NULL,
	`valid` TINYINT UNSIGNED NOT NULL,
	`amount` INT NOT NULL,
    `character_inventoryID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PRIMARY KEY(`character_inventoryID`),
    INDEX(`character`(16))
) ENGINE = InnoDB;
    
-- DROP TABLE character_equipment;

CREATE TABLE IF NOT EXISTS `character_equipment` (
	`character` VARCHAR(16) NOT NULL,
	`slot` INT NOT NULL,
	`name` VARCHAR(50) NOT NULL,
	`valid` TINYINT UNSIGNED NOT NULL,
	`amount` INT NOT NULL,
    `character_equipmentID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PRIMARY KEY(`character_equipmentID`),
    INDEX(`character`(16))
) ENGINE = InnoDB;

-- DROP TABLE character_skills;

CREATE TABLE IF NOT EXISTS `character_skills` (
	`character` VARCHAR(16) NOT NULL,
	`name` VARCHAR(50) NOT NULL,
	`learned` TINYINT UNSIGNED NOT NULL,
	`level` INT NOT NULL,
	`castTimeEnd` FLOAT NOT NULL,
	`cooldownEnd` FLOAT NOT NULL,
	`buffTimeEnd` FLOAT NOT NULL,
    `character_skillsID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PRIMARY KEY(`character_skillsID`),
    INDEX(`character`(16))
) ENGINE = InnoDB;
    
-- DROP TABLE character_quests;

CREATE TABLE IF NOT EXISTS `character_quests` (
	`character` VARCHAR(16) NOT NULL,
	`name` VARCHAR(50) NOT NULL,
	`killed` INT NOT NULL,
	`completed` TINYINT UNSIGNED NOT NULL,
    `character_questsID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PRIMARY KEY(`character_questsID`),
    INDEX(`character`(16))
) ENGINE = InnoDB;

-- DROP TABLE character_orders;

CREATE TABLE IF NOT EXISTS `character_orders` (
	`orderid` BIGINT NOT NULL AUTO_INCREMENT,
    `character` VARCHAR(16) NOT NULL,
    `coins` BIGINT NOT NULL,
    `processed` BIGINT NOT NULL,
    PRIMARY KEY(`orderid`),
    INDEX(`character`(16))
) ENGINE = InnoDB;	
	
-- DROP TABLE accounts;

CREATE TABLE IF NOT EXISTS `accounts` (
	`name` VARCHAR(16) NOT NULL,
	`password` CHAR(40) NOT NULL,
	`banned` TINYINT UNSIGNED NOT NULL,
	`warehouse_gold` BIGINT NOT NULL DEFAULT '0',
    PRIMARY KEY(`name`)
) ENGINE = InnoDB;
      
-- DROP TABLE `warehouse_player`;

CREATE TABLE IF NOT EXISTS `warehouse_player` (
	`account` VARCHAR(16) NOT NULL,
	`slot` INT NOT NULL,
	`name` VARCHAR(16) NOT NULL,
	`valid` TINYINT UNSIGNED NOT NULL,
	`amount` INT NOT NULL,
    PRIMARY KEY(`account`)
) ENGINE = InnoDB;
    
-- DROP TABLE `guild_info`;

CREATE TABLE IF NOT EXISTS `guild_info` (
	`name` VARCHAR(16) NOT NULL,
	`notice` TEXT NOT NULL,
	PRIMARY KEY(`name`)
) ENGINE = InnoDB;

-- DROP TABLE `guild_members`;

CREATE TABLE IF NOT EXISTS `guild_members` (
	`guild` VARCHAR(16) NOT NULL,
	`character` VARCHAR(16) NOT NULL,
	`rank` INT NOT NULL,  
  	PRIMARY KEY(`guild`)
) ENGINE = InnoDB;

-- DROP TABLE `warehouse_guild`;

CREATE TABLE IF NOT EXISTS `warehouse_guild` (
	`guild` VARCHAR(16) NOT NULL,
	`slot` INT NOT NULL,
	`name` VARCHAR(16) NOT NULL,
	`valid` TINYINT UNSIGNED NOT NULL,
	`amount` INT NOT NULL,
    PRIMARY KEY(`guild`)
) ENGINE = InnoDB;  
    
-- ---------------------------------------------------------------------------------------
